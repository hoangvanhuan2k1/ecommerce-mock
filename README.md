# Getting Started

First, run the development server:

```bash
#B1
npm install --force
# or
 yarn install
# or
 bun install

#B2
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```
